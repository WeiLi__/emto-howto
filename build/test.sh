#! /bin/bash

for folder in bmdl-mdl kstr-tfh shape-shp
do
		cd ${folder/-*/}
		pwd
		[ -e */${1:-'fcc'}.${folder/*-/} ] && echo ${1:-'fcc'}.${folder/*-/} is exist
		[ -e */${1:-'fcc'}.${folder/*-/} ] || ./run ${1:-'fcc'}
		cd ..
done


cd kgrn
pwd
echo "the test for Cu"
echo "the test input files:"
echo "cu.dat will be run as parallel and use STMP=A "
echo "cu-s.dat will be ren as serial and use STMP=Y , but tmp file will be save in memory disc"
diff -y  --left-column cu.dat cu-s.dat

echo ""

echo "start runing the parallel EMTO for cu.dat ... "
rm -f cu.prn cu.dos
time ../bin/kgrn_omp < cu.dat
echo "start running the serial EMTO for cu-s.dat ..."
rm -f cu-s.prn cu-s.dos
time ../bin/kgrn_cpa < cu-s.dat
echo difference of the dos:
echo cu.dos           cu-s.dos
diff -y --suppress-common-lines cu.dos cu-s.dos                  
echo "" 
cd ../kfcd
pwd
echo "start running kfcd ..."
rm -f cu.prn
rm -f cu-s.prn

time ../bin/kfcd_cpa < cu.dat &
time ../bin/kfcd_cpa < cu-s.dat &
wait

echo "for OpenMP parallel EMTO of cu.dat"
grep TOT- cu.prn
echo "for serial EMTO of cu-s.dat"
grep TOT- cu-s.prn

#! /bin/bash

cat <<EOF
       subroutine gitinf()
       m6=7
       write(m6,*)''
       write(m6,*)'current branch on: $(git log -1 --format='%d' 2>/dev/null)'
       write(m6,*)'                   $(git symbolic-ref --short HEAD 2>/dev/null)'
       write(m6,*)'  commit hash key: $(git rev-parse --verify HEAD 2>/dev/null)'
       write(m6,*)'       compile on: $(date +%d-%b-%y-%R 2>/dev/null)'
       write(m6,*)'          OS info: $(uname -oisr 2>/dev/null)'
       write(m6,*)'         CPU-info: $(cat /proc/cpuinfo 2>/dev/null|grep -m1 'model *name'|awk '{print $4,$5,$6,$7,$8,$9,$10}')'
       write(m6,*)''
       end subroutine

EOF
       #write(m6,*)'    compiler flags: $(cat make.inc  2>/dev/null|sed -e '/^$/d' )''
